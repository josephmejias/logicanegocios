/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaOlimpico.logicanegocios.bl.logic;

import medallistaOlimpico.logicanegocios.bl.entities.Provincia;
import medallistaOlimpico.logicanegocios.bl.entities.Atleta;
import medallistaOlimpico.logicanegocios.bl.entities.Actividad;
import medallistaOlimpico.logicanegocios.bl.entities.Locacion;
import medallistaOlimpico.logicanegocios.bl.entities.Reto;
import medallistaOlimpico.logicanegocios.bl.entities.Grupo;
import medallistaOlimpico.logicanegocios.bl.entities.Direccion;
import medallistaOlimpico.logicanegocios.bl.entities.MetodoPago;
import medallistaOlimpico.logicanegocios.bl.entities.Canton;
import medallistaOlimpico.logicanegocios.bl.entities.Medalla;
import medallistaOlimpico.logicanegocios.bl.entities.Distrito;
import medallistaOlimpico.logicanegocios.bl.entities.Administrador;
import medallistaOlimpico.logicanegocios.bl.entities.Hito;
import medallistaOlimpico.logicanegocios.bl.entities.Pais;
import medallistaOlimpico.logicanegocios.bl.entities.Transaccion;
import java.time.LocalDate;
import java.util.ArrayList;
import medallistaOlimpico.logicanegocios.dl.Data;

public class Gestor {

    final private Data data;

    public Gestor() {
        data = new Data();
    }

    public void registrarActividad(int idActividad, String codigo, String nombre, String rutaIcono) {
        data.saveActividad(new Actividad(idActividad, codigo, nombre, rutaIcono));
    }

    public void registrarAdministrador(int idPersona, String nombre, String segundoNombre, String apellidos, String cedula, Pais pais, String correo, String clave) {
        data.saveAdministrador(new Administrador(idPersona, nombre, segundoNombre, apellidos, cedula, pais, correo, clave));
    }

    public void registrarAtleta(LocalDate fechaNacimiento, Direccion direccion, String genero, int idPersona, String nombre, String segundoNombre, String apellidos, String cedula, Pais pais, String correo, String clave) {

        data.saveAtleta(new Atleta(fechaNacimiento, direccion, genero, idPersona, nombre, segundoNombre, apellidos, cedula, pais, correo, clave));
    }

    public void registrarCanton(Provincia provincia, int idLocacion, String nombre, String codigo) {
        data.saveCanton(new Canton(provincia, idLocacion, nombre, codigo));
    }

    public void registrarDireccion(Pais pais, Provincia provincia, Canton canton, Distrito distrito, String nombreDireccion, String linea1, String linea2) {
        data.saveDireccion(new Direccion(pais, provincia, canton, distrito, nombreDireccion, linea1, linea2));
    }

    public void registrarDistrito(Canton canton, int idLocacion, String nombre, String codigo) {
        data.saveDistrito(new Distrito(canton, idLocacion, nombre, codigo));
    }

    public void registrarGrupo(int idGrupo, String nombreGrupo, ArrayList<Atleta> miembros, Reto reto, LocalDate fechaRegistro) {
        data.saveGrupo(new Grupo(idGrupo, nombreGrupo, miembros, reto, fechaRegistro));
    }

    public void registrarHito(int idHito, Reto reto, int numHito, String rutaImagen, String descripcion, String[] coordenadas) {
        data.saveHito(new Hito(idHito, reto, numHito, rutaImagen, descripcion, coordenadas));
    }

    public void registrarMedalla(int idMedalla, String nombre, String rutaImagen, String descripcion) {
        data.saveMedalla(new Medalla(idMedalla, nombre, rutaImagen, descripcion));
    }

    public void registrarMetodoPago(int idMetodoPago, String nombreTarjeta, int numeroTarjeta, int mesExpiracion, int anioExpiracion) {
        data.saveMetodoPago(new MetodoPago(idMetodoPago, nombreTarjeta, numeroTarjeta, mesExpiracion, anioExpiracion));
    }

    public void registrarPais(int idLocacion, String nombre, String codigo) {
        data.savePais(new Pais(idLocacion, nombre, codigo));
    }

    public void registrarProvincia(Pais pais, int idLocacion, String nombre, String codigo) {
        data.saveProvincia(new Provincia(pais, idLocacion, nombre, codigo));
    }

    public void registrarReto(int idReto, String nombre, String descripcion, String[] coordenadas, Actividad actividad, Locacion locacion, Medalla medalla, String codigoAcceso) {
        data.saveReto(new Reto(idReto, nombre, descripcion, coordenadas, actividad, locacion, medalla, codigoAcceso));
    }

    public void registrarTransaccion(int idTransaccion, MetodoPago pago, Atleta atleta, LocalDate fecha, String estado, String tipoCambio, double monto, String tipoTransaccion) {
        data.saveTransaccion(new Transaccion(idTransaccion, pago, atleta, fecha, estado, tipoCambio, monto, tipoTransaccion));
    }

    public ArrayList<Actividad> listarActividades() {
        return data.getDataActividades();
    }

    public ArrayList<Administrador> listarAdministrador() {
        return data.getDataAdmin();
    }

    public ArrayList<Atleta> listarAtletas() {
        return data.getDataAtletas();
    }

    public ArrayList<Canton> listarCantones() {
        return data.getDataCantones();
    }

    public ArrayList<Direccion> listarDirecciones() {
        return data.getDataDirecciones();
    }

    public ArrayList<Distrito> listarDistritos() {
        return data.getDataDistritos();
    }

    public ArrayList<Grupo> listarGrupos() {
        return data.getDataGrupos();
    }

    public ArrayList<Hito> listarHitos() {
        return data.getDataHitos();
    }

    public ArrayList<Medalla> listarMedallas() {
        return data.getDataMedallas();
    }

    public ArrayList<MetodoPago> listarMetodoPagos() {
        return data.getDataMetodosPago();
    }

    public ArrayList<Pais> listarPaises() {
        return data.getDataPaises();
    }

    public ArrayList<Provincia> listarProvincias() {
        return data.getDataProvincias();
    }

    public ArrayList<Reto> listarRetos() {
        return data.getDataRetos();
    }

    public ArrayList<Transaccion> listarTransacciones() {
        return data.getDataTransacciones();
    }

}
