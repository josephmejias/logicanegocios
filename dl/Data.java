/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaOlimpico.logicanegocios.dl;

import medallistaOlimpico.logicanegocios.bl.entities.Provincia;
import medallistaOlimpico.logicanegocios.bl.entities.Atleta;
import medallistaOlimpico.logicanegocios.bl.entities.Actividad;
import medallistaOlimpico.logicanegocios.bl.entities.Reto;
import medallistaOlimpico.logicanegocios.bl.entities.Grupo;
import medallistaOlimpico.logicanegocios.bl.entities.Direccion;
import medallistaOlimpico.logicanegocios.bl.entities.MetodoPago;
import medallistaOlimpico.logicanegocios.bl.entities.Canton;
import medallistaOlimpico.logicanegocios.bl.entities.Medalla;
import medallistaOlimpico.logicanegocios.bl.entities.Distrito;
import medallistaOlimpico.logicanegocios.bl.entities.Administrador;
import medallistaOlimpico.logicanegocios.bl.entities.Hito;
import medallistaOlimpico.logicanegocios.bl.entities.Pais;
import medallistaOlimpico.logicanegocios.bl.entities.Transaccion;
import java.util.ArrayList;

public class Data {

    private ArrayList<Administrador> dataAdmin = new ArrayList();
    private ArrayList<Actividad> dataActividades = new ArrayList();
    private ArrayList<Atleta> dataAtletas = new ArrayList();
    private ArrayList<Canton> dataCantones = new ArrayList();
    private ArrayList<Direccion> dataDirecciones = new ArrayList();
    private ArrayList<Distrito> dataDistritos = new ArrayList();
    private ArrayList<Grupo> dataGrupos = new ArrayList();
    private ArrayList<Hito> dataHitos = new ArrayList();
    private ArrayList<Medalla> dataMedallas = new ArrayList();
    private ArrayList<MetodoPago> dataMetodosPago = new ArrayList();
    private ArrayList<Pais> dataPaises = new ArrayList();
    private ArrayList<Provincia> dataProvincias = new ArrayList();
    private ArrayList<Reto> dataRetos = new ArrayList();
    private ArrayList<Transaccion> dataTransacciones = new ArrayList();

    public Data() {
    }

    public void saveAtleta(Atleta atleta) {
        dataAtletas.add(atleta);
    }

    public void saveActividad(Actividad actividad) {
        dataActividades.add(actividad);
    }

    public void saveAdministrador(Administrador administrador) {
        dataAdmin.add(administrador);
    }

    public void saveCanton(Canton canton) {
        dataCantones.add(canton);
    }

    public void saveDireccion(Direccion direccion) {
        dataDirecciones.add(direccion);
    }

    public void saveDistrito(Distrito distrito) {
        dataDistritos.add(distrito);
    }

    public void saveGrupo(Grupo grupo) {
        dataGrupos.add(grupo);
    }

    public void saveHito(Hito hito) {
        dataHitos.add(hito);
    }

    public void saveMedalla(Medalla medalla) {
        dataMedallas.add(medalla);
    }

    public void saveMetodoPago(MetodoPago metodoPago) {
        dataMetodosPago.add(metodoPago);
    }

    public void savePais(Pais pais) {
        dataPaises.add(pais);
    }

    public void saveProvincia(Provincia provincia) {
        dataProvincias.add(provincia);
    }

    public void saveReto(Reto reto) {
        dataRetos.add(reto);
    }

    public void saveTransaccion(Transaccion transaccion) {
        dataTransacciones.add(transaccion);
    }

    /**
     * @return the dataAdmin
     */
    public ArrayList<Administrador> getDataAdmin() {
        return (ArrayList) dataAdmin.clone();
    }

    /**
     * @param dataAdmin the dataAdmin to set
     */
    public void setDataAdmin(ArrayList<Administrador> dataAdmin) {
        this.dataAdmin = dataAdmin;
    }

    /**
     * @return the dataActividades
     */
    public ArrayList<Actividad> getDataActividades() {
        return (ArrayList) dataActividades.clone();
    }

    /**
     * @param dataActividades the dataActividades to set
     */
    public void setDataActividades(ArrayList<Actividad> dataActividades) {
        this.dataActividades = dataActividades;
    }

    /**
     * @return the dataAtletas
     */
    public ArrayList<Atleta> getDataAtletas() {
        return (ArrayList) dataAtletas.clone();
    }

    /**
     * @param dataAtletas the dataAtletas to set
     */
    public void setDataAtletas(ArrayList<Atleta> dataAtletas) {
        this.dataAtletas = dataAtletas;
    }

    /**
     * @return the dataCantones
     */
    public ArrayList<Canton> getDataCantones() {
        return (ArrayList) dataCantones.clone();
    }

    /**
     * @param dataCantones the dataCantones to set
     */
    public void setDataCantones(ArrayList<Canton> dataCantones) {
        this.dataCantones = dataCantones;
    }

    /**
     * @return the dataDirecciones
     */
    public ArrayList<Direccion> getDataDirecciones() {
        return (ArrayList) dataDirecciones.clone();
    }

    /**
     * @param dataDirecciones the dataDirecciones to set
     */
    public void setDataDirecciones(ArrayList<Direccion> dataDirecciones) {
        this.dataDirecciones = dataDirecciones;
    }

    /**
     * @return the dataDistritos
     */
    public ArrayList<Distrito> getDataDistritos() {
        return (ArrayList) dataDistritos.clone();
    }

    /**
     * @param dataDistritos the dataDistritos to set
     */
    public void setDataDistritos(ArrayList<Distrito> dataDistritos) {
        this.dataDistritos = dataDistritos;
    }

    /**
     * @return the dataGrupos
     */
    public ArrayList<Grupo> getDataGrupos() {
        return (ArrayList) dataGrupos.clone();
    }

    /**
     * @param dataGrupos the dataGrupos to set
     */
    public void setDataGrupos(ArrayList<Grupo> dataGrupos) {
        this.dataGrupos = dataGrupos;
    }

    /**
     * @return the dataHitos
     */
    public ArrayList<Hito> getDataHitos() {
        return (ArrayList) dataHitos.clone();
    }

    /**
     * @param dataHitos the dataHitos to set
     */
    public void setDataHitos(ArrayList<Hito> dataHitos) {
        this.dataHitos = dataHitos;
    }

    /**
     * @return the dataMedallas
     */
    public ArrayList<Medalla> getDataMedallas() {
        return (ArrayList) dataMedallas.clone();
    }

    /**
     * @param dataMedallas the dataMedallas to set
     */
    public void setDataMedallas(ArrayList<Medalla> dataMedallas) {
        this.dataMedallas = dataMedallas;
    }

    /**
     * @return the dataMetodosPagos
     */
    public ArrayList<MetodoPago> getDataMetodosPago() {
        return (ArrayList) dataMetodosPago.clone();
    }

    /**
     * @param dataMetodosPago the dataMetodosPagos to set
     */
    public void setDataMetodosPago(ArrayList<MetodoPago> dataMetodosPago) {
        this.dataMetodosPago = dataMetodosPago;
    }

    /**
     * @return the dataPaises
     */
    public ArrayList<Pais> getDataPaises() {
        return (ArrayList) dataPaises.clone();
    }

    /**
     * @param dataPaises the dataPaises to set
     */
    public void setDataPaises(ArrayList<Pais> dataPaises) {
        this.dataPaises = dataPaises;
    }

    /**
     * @return the dataProvincias
     */
    public ArrayList<Provincia> getDataProvincias() {
        return (ArrayList) dataProvincias.clone();
    }

    /**
     * @param dataProvincias the dataProvincias to set
     */
    public void setDataProvincias(ArrayList<Provincia> dataProvincias) {
        this.dataProvincias = dataProvincias;
    }

    /**
     * @return the dataRetos
     */
    public ArrayList<Reto> getDataRetos() {
        return (ArrayList) dataRetos.clone();
    }

    /**
     * @param dataRetos the dataRetos to set
     */
    public void setDataRetos(ArrayList<Reto> dataRetos) {
        this.dataRetos = dataRetos;
    }

    /**
     * @return the dataTransacciones
     */
    public ArrayList<Transaccion> getDataTransacciones() {
        return (ArrayList) dataTransacciones.clone();
    }

    /**
     * @param dataTransacciones the dataTransacciones to set
     */
    public void setDataTransacciones(ArrayList<Transaccion> dataTransacciones) {
        this.dataTransacciones = dataTransacciones;
    }

}
